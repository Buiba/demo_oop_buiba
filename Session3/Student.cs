﻿using System;

namespace Session3
{
    /// <summary>
    ///Create class student descendants of person.
    /// </summary>
    class Student : Person
    {
        /// <summary>
        ///Method to display GotoClasses message.
        /// </summary>
        public void GotoClasses()
        {
            Console.WriteLine("I'm going to class.");
        }
        /// <summary>
        ///Method to show age of students.
        /// </summary>
        public void ShowAge()
        {
            Console.WriteLine("My age is: {0} years old.", _age);
        }
    }
}

