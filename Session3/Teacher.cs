﻿using System;

namespace Session3
{
    /// <summary>
    /// Create class teacher descendants of person.
    /// </summary>
    class Teacher:Person
    {
        private string _subject;
        /// <summary>
        ///Method to update the value for the _subject.
        /// </summary>
        /// <param name="subject">the subject.</param>
        public void SetSubject(string subject)
        {
            _subject = subject;
        }
        /// <summary>
        ///Method to display Explain message.
        /// </summary>
        public void Explain()
        {
            Console.WriteLine("Explanation begin.");
        }
    }
}
