﻿using System;

namespace Session3
{
    /// <summary>
    ///  Create class person.
    /// </summary>
    class Person
    {
        protected int _age;
        /// <summary>
        /// Method to display Greeting message.
        /// </summary>
        public void Greeting()
        {
            Console.WriteLine("Hello!");
        }
        /// <summary>
        /// Method to update the value for the _age.
        /// </summary>
        /// <param name="age">The age</param>
        public void SetAge(int age)
        {
            _age = age;
        }        
    }
}
