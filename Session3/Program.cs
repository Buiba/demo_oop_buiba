﻿using System;

namespace Session3
{
    class Program
    {
        static void Main(string[] args)
        {
            Person per = new Person();
            per.Greeting();

            Student std = new Student();
            std.SetAge(21);
            std.GotoClasses();
            std.ShowAge();

            Teacher tea = new Teacher();
            tea.SetAge(30);
            tea.SetSubject("Math");
            tea.Explain();

            Console.ReadKey();
        }
    }
}
